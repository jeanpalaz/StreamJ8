package fr.palazuelos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

public class MyStreamShould {

	private Stream<String> stream;
	
	@Before	public void 
	setUp() throws IOException {
			stream = Files.lines(Paths.get("test/ressources/manifesto.txt")) ;
	}
	
	@Test public void
	returnFirstLine() {
	}
	
	@Test public void
	countLines() {
	}
	
	@Test public void
	returnLinesWithLogiciels() {
		//stream.
	}
	
	@Test public void
	returnFileWithoutEmptyLines() {
		//stream.
	}
}
